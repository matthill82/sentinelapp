3.2.1 (Media Mark)
9e9e2e6ee751b7ee4471ef0edf560eab5b63b5f5
o:Sass::Tree::RootNode
:
@linei:@options{ :@template"
/**
 * Typography
 * --------------------------------------------------
 */


// Body text
// -------------------------

p {
  margin: 0 0 ($line-height-computed / 2);
}


// Emphasis & misc
// -------------------------

small   { font-size: 85%; }
cite    { font-style: normal; }


// Alignment
// -------------------------

.text-left           { text-align: left; }
.text-right          { text-align: right; }
.text-center         { text-align: center; }


// Headings
// -------------------------

h1, h2, h3, h4, h5, h6,
.h1, .h2, .h3, .h4, .h5, .h6 {
  color: $base-color;
  font-weight: $headings-font-weight;
  font-family: $headings-font-family;
  line-height: $headings-line-height;

  small {
    font-weight: normal;
    line-height: 1;
  }
}

h1, .h1,
h2, .h2,
h3, .h3 {
  margin-top: $line-height-computed;
  margin-bottom: ($line-height-computed / 2);

  &:first-child {
    margin-top: 0;
  }

  + h1, + .h1,
  + h2, + .h2,
  + h3, + .h3 {
    margin-top: ($line-height-computed / 2);
  }
}

h4, .h4,
h5, .h5,
h6, .h6 {
  margin-top: ($line-height-computed / 2);
  margin-bottom: ($line-height-computed / 2);
}

h1, .h1 { font-size: floor($font-size-base * 2.60); } // ~36px
h2, .h2 { font-size: floor($font-size-base * 2.15); } // ~30px
h3, .h3 { font-size: ceil($font-size-base * 1.70); } // ~24px
h4, .h4 { font-size: ceil($font-size-base * 1.25); } // ~18px
h5, .h5 { font-size:  $font-size-base; }
h6, .h6 { font-size: ceil($font-size-base * 0.85); } // ~12px

h1 small, .h1 small { font-size: ceil($font-size-base * 1.70); } // ~24px
h2 small, .h2 small { font-size: ceil($font-size-base * 1.25); } // ~18px
h3 small, .h3 small,
h4 small, .h4 small { font-size: $font-size-base; }


// Description Lists
// -------------------------

dl {
  margin-bottom: $line-height-computed;
}
dt,
dd {
  line-height: $line-height-base;
}
dt {
  font-weight: bold;
}


// Blockquotes
// -------------------------

blockquote {
  margin: 0 0 $line-height-computed;
  padding: ($line-height-computed / 2) $line-height-computed;
  border-left: 5px solid gray;
  
  p {
    font-weight: 300;
    font-size: ($font-size-base * 1.25);
    line-height: 1.25;
  }
  
  p:last-child {
    margin-bottom: 0;
  }

  small {
    display: block;
    line-height: $line-height-base;
    &:before {
      content: '\2014 \00A0';// EM DASH, NBSP;
    }
  }
}


// Quotes
// -------------------------

q:before,
q:after,
blockquote:before,
blockquote:after {
  content: "";
}


// Addresses
// -------------------------

address {
  display: block;
  margin-bottom: $line-height-computed;
  font-style: normal;
  line-height: $line-height-base;
}


// Links
// -------------------------

a.subdued {
  padding-right: 10px;
  color: #888;
  text-decoration: none;

  &:hover {
    text-decoration: none;
  }
  &:last-child {
    padding-right: 0;
  }
}
:@has_childrenT:@children[/o:Sass::Tree::CommentNode
;i;@;
[ :
@type:normal:@value["P/**
 * Typography
 * --------------------------------------------------
 */o;
;i;@;
[ ;:silent;["1/* Body text
 * ------------------------- */o:Sass::Tree::RuleNode:
@tabsi ;@:
@rule["p:@parsed_ruleso:"Sass::Selector::CommaSequence:@filename" ;i:@members[o:Sass::Selector::Sequence;[o:#Sass::Selector::SimpleSequence
;@;i:@subject0;[o:Sass::Selector::Element	;@:@namespace0:
@name["p;i:@sourceso:Set:
@hash{ ;	T;i;
[o:Sass::Tree::PropNode;i ;["margin;@:@prop_syntax:new;o:Sass::Script::List	;i;@:@separator:
space;[o:Sass::Script::Number:@numerator_units[ ;i;@:@original"0;i :@denominator_units[ o;&;'[ ;i;@;("0;i ;)@)o:Sass::Script::Operation
:@operator:div;i;@:@operand1o:Sass::Script::Variable	;i;"line-height-computed;@:@underscored_name"line_height_computed:@operand2o;&;'[ ;i;@;("2;i;)@);i;
[ o;
;i;@;
[ ;;;["7/* Emphasis & misc
 * ------------------------- */o;;i ;@;["
small;o;;" ;i;[o;;[o;
;@=;i;0;[o;	;@=;0;["
small;i;o;;{ ;	T;i;
[o; ;i ;["font-size;@;!;";o:Sass::Script::String;@;"85%;:identifier;i;
[ o;;i ;@;["	cite;o;;" ;i;[o;;[o;
;@S;i;0;[o;	;@S;0;["	cite;i;o;;{ ;	T;i;
[o; ;i ;["font-style;@;!;";o;1;@;"normal;;2;i;
[ o;
;i;@;
[ ;;;["1/* Alignment
 * ------------------------- */o;;i ;@;[".text-left;o;;" ;i;[o;;[o;
;@m;i;0;[o:Sass::Selector::Class;@m;["text-left;i;o;;{ ;	T;i;
[o; ;i ;["text-align;@;!;";o;1;@;"	left;;2;i;
[ o;;i ;@;[".text-right;o;;" ;i ;[o;;[o;
;@~;i ;0;[o;3;@~;["text-right;i ;o;;{ ;	T;i ;
[o; ;i ;["text-align;@;!;";o;1;@;"
right;;2;i ;
[ o;;i ;@;[".text-center;o;;" ;i!;[o;;[o;
;@�;i!;0;[o;3;@�;["text-center;i!;o;;{ ;	T;i!;
[o; ;i ;["text-align;@;!;";o;1;@;"center;;2;i!;
[ o;
;i$;@;
[ ;;;["0/* Headings
 * ------------------------- */o;;i ;@;["9h1, h2, h3, h4, h5, h6,
.h1, .h2, .h3, .h4, .h5, .h6;o;;" ;i(;[o;;[o;
;@�;i(;0;[o;	;@�;0;["h1;i(;o;;{ o;;[o;
;@�;i(;0;[o;	;@�;0;["h2;i(;o;;{ o;;[o;
;@�;i(;0;[o;	;@�;0;["h3;i(;o;;{ o;;[o;
;@�;i(;0;[o;	;@�;0;["h4;i(;o;;{ o;;[o;
;@�;i(;0;[o;	;@�;0;["h5;i(;o;;{ o;;[o;
;@�;i(;0;[o;	;@�;0;["h6;i(;o;;{ o;;["
o;
;@�;i(;0;[o;3;@�;["h1;i(;o;;{ o;;[o;
;@�;i(;0;[o;3;@�;["h2;i(;o;;{ o;;[o;
;@�;i(;0;[o;3;@�;["h3;i(;o;;{ o;;[o;
;@�;i(;0;[o;3;@�;["h4;i(;o;;{ o;;[o;
;@�;i(;0;[o;3;@�;["h5;i(;o;;{ o;;[o;
;@�;i(;0;[o;3;@�;["h6;i(;o;;{ ;	T;i(;
[
o; ;i ;["
color;@;!;";o;.	;i);"base-color;@;/"base_color;i);
[ o; ;i ;["font-weight;@;!;";o;.	;i*;"headings-font-weight;@;/"headings_font_weight;i*;
[ o; ;i ;["font-family;@;!;";o;.	;i+;"headings-font-family;@;/"headings_font_family;i+;
[ o; ;i ;["line-height;@;!;";o;.	;i,;"headings-line-height;@;/"headings_line_height;i,;
[ o;;i ;@;["
small;o;;" ;i.;[o;;[o;
;@>;i.;0;[o;	;@>;0;["
small;i.;o;;{ ;	T;i.;
[o; ;i ;["font-weight;@;!;";o;1;@;"normal;;2;i/;
[ o; ;i ;["line-height;@;!;";o;1;@;"1;;2;i0;
[ o;;i ;@;["h1, .h1,
h2, .h2,
h3, .h3;o;;" ;i6;[o;;[o;
;@Z;i6;0;[o;	;@Z;0;["h1;i6;o;;{ o;;[o;
;@Z;i6;0;[o;3;@Z;["h1;i6;o;;{ o;;["
o;
;@Z;i6;0;[o;	;@Z;0;["h2;i6;o;;{ o;;[o;
;@Z;i6;0;[o;3;@Z;["h2;i6;o;;{ o;;["
o;
;@Z;i6;0;[o;	;@Z;0;["h3;i6;o;;{ o;;[o;
;@Z;i6;0;[o;3;@Z;["h3;i6;o;;{ ;	T;i6;
[	o; ;i ;["margin-top;@;!;";o;.	;i7;"line-height-computed;@;/"line_height_computed;i7;
[ o; ;i ;["margin-bottom;@;!;";o;*
;+;,;i8;@;-o;.	;i8;"line-height-computed;@;/"line_height_computed;0o;&;'[ ;i8;@;("2;i;)@);i8;
[ o;;i ;@;["&:first-child;o;;" ;i:;[o;;[o;
;@�;i:;0;[o:Sass::Selector::Parent;@�;i:o:Sass::Selector::Pseudo
;@�;["first-child;i:;:
class:	@arg0;o;;{ ;	T;i:;
[o; ;i ;["margin-top;@;!;";o;1;@;"0;;2;i;;
[ o;;i ;@;[".+ h1, + .h1,
  + h2, + .h2,
  + h3, + .h3;o;;" ;i@;[o;;["+o;
;@�;i@;0;[o;	;@�;0;["h1;i@;o;;{ o;;["+o;
;@�;i@;0;[o;3;@�;["h1;i@;o;;{ o;;["
"+o;
;@�;i@;0;[o;	;@�;0;["h2;i@;o;;{ o;;["+o;
;@�;i@;0;[o;3;@�;["h2;i@;o;;{ o;;["
"+o;
;@�;i@;0;[o;	;@�;0;["h3;i@;o;;{ o;;["+o;
;@�;i@;0;[o;3;@�;["h3;i@;o;;{ ;	T;i@;
[o; ;i ;["margin-top;@;!;";o;*
;+;,;iA;@;-o;.	;iA;"line-height-computed;@;/"line_height_computed;0o;&;'[ ;iA;@;("2;i;)@);iA;
[ o;;i ;@;["h4, .h4,
h5, .h5,
h6, .h6;o;;" ;iG;[o;;[o;
;@;iG;0;[o;	;@;0;["h4;iG;o;;{ o;;[o;
;@;iG;0;[o;3;@;["h4;iG;o;;{ o;;["
o;
;@;iG;0;[o;	;@;0;["h5;iG;o;;{ o;;[o;
;@;iG;0;[o;3;@;["h5;iG;o;;{ o;;["
o;
;@;iG;0;[o;	;@;0;["h6;iG;o;;{ o;;[o;
;@;iG;0;[o;3;@;["h6;iG;o;;{ ;	T;iG;
[o; ;i ;["margin-top;@;!;";o;*
;+;,;iH;@;-o;.	;iH;"line-height-computed;@;/"line_height_computed;0o;&;'[ ;iH;@;("2;i;)@);iH;
[ o; ;i ;["margin-bottom;@;!;";o;*
;+;,;iI;@;-o;.	;iI;"line-height-computed;@;/"line_height_computed;0o;&;'[ ;iI;@;("2;i;)@);iI;
[ o;;i ;@;["h1, .h1;o;;" ;iL;[o;;[o;
;@g;iL;0;[o;	;@g;0;["h1;iL;o;;{ o;;[o;
;@g;iL;0;[o;3;@g;["h1;iL;o;;{ ;	T;iL;
[o; ;i ;["font-size;@;!;";o:Sass::Script::Funcall:
@args[o;*
;+:
times;iL;@;-o;.	;iL;"font-size-base;@;/"font_size_base;0o;&;'[ ;iL;@;("2.6;f2.6;)@);"
floor;iL;@:@splat0:@keywords{ ;iL;
[ o;
;iL;@;
[ ;;;["/* ~36px */o;;i ;@;["h2, .h2;o;;" ;iM;[o;;[o;
;@�;iM;0;[o;	;@�;0;["h2;iM;o;;{ o;;[o;
;@�;iM;0;[o;3;@�;["h2;iM;o;;{ ;	T;iM;
[o; ;i ;["font-size;@;!;";o;8;9[o;*
;+;:;iM;@;-o;.	;iM;"font-size-base;@;/"font_size_base;0o;&;'[ ;iM;@;("	2.15;f	2.15;)@);"
floor;iM;@;;0;<{ ;iM;
[ o;
;iM;@;
[ ;;;["/* ~30px */o;;i ;@;["h3, .h3;o;;" ;iN;[o;;[o;
;@�;iN;0;[o;	;@�;0;["h3;iN;o;;{ o;;[o;
;@�;iN;0;[o;3;@�;["h3;iN;o;;{ ;	T;iN;
[o; ;i ;["font-size;@;!;";o;8;9[o;*
;+;:;iN;@;-o;.	;iN;"font-size-base;@;/"font_size_base;0o;&;'[ ;iN;@;("1.7;f1.7;)@);"	ceil;iN;@;;0;<{ ;iN;
[ o;
;iN;@;
[ ;;;["/* ~24px */o;;i ;@;["h4, .h4;o;;" ;iO;[o;;[o;
;@�;iO;0;[o;	;@�;0;["h4;iO;o;;{ o;;[o;
;@�;iO;0;[o;3;@�;["h4;iO;o;;{ ;	T;iO;
[o; ;i ;["font-size;@;!;";o;8;9[o;*
;+;:;iO;@;-o;.	;iO;"font-size-base;@;/"font_size_base;0o;&;'[ ;iO;@;("	1.25;f	1.25;)@);"	ceil;iO;@;;0;<{ ;iO;
[ o;
;iO;@;
[ ;;;["/* ~18px */o;;i ;@;["h5, .h5;o;;" ;iP;[o;;[o;
;@;iP;0;[o;	;@;0;["h5;iP;o;;{ o;;[o;
;@;iP;0;[o;3;@;["h5;iP;o;;{ ;	T;iP;
[o; ;i ;["font-size;@;!;";o;.	;iP;"font-size-base;@;/"font_size_base;iP;
[ o;;i ;@;["h6, .h6;o;;" ;iQ;[o;;[o;
;@;;iQ;0;[o;	;@;;0;["h6;iQ;o;;{ o;;[o;
;@;;iQ;0;[o;3;@;;["h6;iQ;o;;{ ;	T;iQ;
[o; ;i ;["font-size;@;!;";o;8;9[o;*
;+;:;iQ;@;-o;.	;iQ;"font-size-base;@;/"font_size_base;0o;&;'[ ;iQ;@;("	0.85;f	0.85;)@);"	ceil;iQ;@;;0;<{ ;iQ;
[ o;
;iQ;@;
[ ;;;["/* ~12px */o;;i ;@;["h1 small, .h1 small;o;;" ;iS;[o;;[o;
;@h;iS;0;[o;	;@h;0;["h1;iS;o;;{ o;
;@h;iS;0;[o;	;@h;0;["
small;iS;o;;{ o;;[o;
;@h;iS;0;[o;3;@h;["h1;iS;o;;{ o;
;@h;iS;0;[o;	;@h;0;["
small;iS;o;;{ ;	T;iS;
[o; ;i ;["font-size;@;!;";o;8;9[o;*
;+;:;iS;@;-o;.	;iS;"font-size-base;@;/"font_size_base;0o;&;'[ ;iS;@;("1.7;f1.7;)@);"	ceil;iS;@;;0;<{ ;iS;
[ o;
;iS;@;
[ ;;;["/* ~24px */o;;i ;@;["h2 small, .h2 small;o;;" ;iT;[o;;[o;
;@�;iT;0;[o;	;@�;0;["h2;iT;o;;{ o;
;@�;iT;0;[o;	;@�;0;["
small;iT;o;;{ o;;[o;
;@�;iT;0;[o;3;@�;["h2;iT;o;;{ o;
;@�;iT;0;[o;	;@�;0;["
small;iT;o;;{ ;	T;iT;
[o; ;i ;["font-size;@;!;";o;8;9[o;*
;+;:;iT;@;-o;.	;iT;"font-size-base;@;/"font_size_base;0o;&;'[ ;iT;@;("	1.25;f	1.25;)@);"	ceil;iT;@;;0;<{ ;iT;
[ o;
;iT;@;
[ ;;;["/* ~18px */o;;i ;@;["-h3 small, .h3 small,
h4 small, .h4 small;o;;" ;iV;[	o;;[o;
;@�;iV;0;[o;	;@�;0;["h3;iV;o;;{ o;
;@�;iV;0;[o;	;@�;0;["
small;iV;o;;{ o;;[o;
;@�;iV;0;[o;3;@�;["h3;iV;o;;{ o;
;@�;iV;0;[o;	;@�;0;["
small;iV;o;;{ o;;["
o;
;@�;iV;0;[o;	;@�;0;["h4;iV;o;;{ o;
;@�;iV;0;[o;	;@�;0;["
small;iV;o;;{ o;;[o;
;@�;iV;0;[o;3;@�;["h4;iV;o;;{ o;
;@�;iV;0;[o;	;@�;0;["
small;iV;o;;{ ;	T;iV;
[o; ;i ;["font-size;@;!;";o;.	;iV;"font-size-base;@;/"font_size_base;iV;
[ o;
;iY;@;
[ ;;;["9/* Description Lists
 * ------------------------- */o;;i ;@;["dl;o;;" ;i\;[o;;[o;
;@1;i\;0;[o;	;@1;0;["dl;i\;o;;{ ;	T;i\;
[o; ;i ;["margin-bottom;@;!;";o;.	;i];"line-height-computed;@;/"line_height_computed;i];
[ o;;i ;@;["dt,
dd;o;;" ;i`;[o;;[o;
;@H;i`;0;[o;	;@H;0;["dt;i`;o;;{ o;;["
o;
;@H;i`;0;[o;	;@H;0;["dd;i`;o;;{ ;	T;i`;
[o; ;i ;["line-height;@;!;";o;.	;ia;"line-height-base;@;/"line_height_base;ia;
[ o;;i ;@;["dt;o;;" ;ic;[o;;[o;
;@i;ic;0;[o;	;@i;0;["dt;ic;o;;{ ;	T;ic;
[o; ;i ;["font-weight;@;!;";o;1;@;"	bold;;2;id;
[ o;
;ih;@;
[ ;;;["3/* Blockquotes
 * ------------------------- */o;;i ;@;["blockquote;o;;" ;ik;[o;;[o;
;@�;ik;0;[o;	;@�;0;["blockquote;ik;o;;{ ;	T;ik;
[o; ;i ;["margin;@;!;";o;#	;il;@;$;%;[o;&;'[ ;il;@;("0;i ;)@)o;&;'[ ;il;@;("0;i ;)@)o;.	;il;"line-height-computed;@;/"line_height_computed;il;
[ o; ;i ;["padding;@;!;";o;#	;im;@;$;%;[o;*
;+;,;im;@;-o;.	;im;"line-height-computed;@;/"line_height_computed;0o;&;'[ ;im;@;("2;i;)@)o;.	;im;"line-height-computed;@;/"line_height_computed;im;
[ o; ;i ;["border-left;@;!;";o;1;@;"5px solid gray;;2;in;
[ o;;i ;@;["p;o;;" ;ip;[o;;[o;
;@�;ip;0;[o;	;@�;0;["p;ip;o;;{ ;	T;ip;
[o; ;i ;["font-weight;@;!;";o;1;@;"300;;2;iq;
[ o; ;i ;["font-size;@;!;";o;*
;+;:;ir;@;-o;.	;ir;"font-size-base;@;/"font_size_base;0o;&;'[ ;ir;@;("	1.25;f	1.25;)@);ir;
[ o; ;i ;["line-height;@;!;";o;1;@;"	1.25;;2;is;
[ o;;i ;@;["p:last-child;o;;" ;iv;[o;;[o;
;@�;iv;0;[o;	;@�;0;["p;ivo;5
;@�;["last-child;iv;;6;70;o;;{ ;	T;iv;
[o; ;i ;["margin-bottom;@;!;";o;1;@;"0;;2;iw;
[ o;;i ;@;["
small;o;;" ;iz;[o;;[o;
;@�;iz;0;[o;	;@�;0;["
small;iz;o;;{ ;	T;iz;
[o; ;i ;["display;@;!;";o;1;@;"
block;;2;i{;
[ o; ;i ;["line-height;@;!;";o;.	;i|;"line-height-base;@;/"line_height_base;i|;
[ o;;i ;@;["&:before;o;;" ;i};[o;;[o;
;@;i};0;[o;4;@;i}o;5
;@;["before;i};;6;70;o;;{ ;	T;i};
[o; ;i ;["content;@;!;";o;1;@;"'\2014 \00A0';;2;i~;
[ o;
;i~;@;
[ ;;;["/* EM DASH, NBSP; */o;
;i;@;
[ ;;;["./* Quotes
 * ------------------------- */o;;i ;@;[";q:before,
q:after,
blockquote:before,
blockquote:after;o;;" ;i�;[	o;;[o;
;@5;i�;0;[o;	;@5;0;["q;i�o;5
;@5;["before;i�;;6;70;o;;{ o;;["
o;
;@5;i�;0;[o;	;@5;0;["q;i�o;5
;@5;["
after;i�;;6;70;o;;{ o;;["
o;
;@5;i�;0;[o;	;@5;0;["blockquote;i�o;5
;@5;["before;i�;;6;70;o;;{ o;;["
o;
;@5;i�;0;[o;	;@5;0;["blockquote;i�o;5
;@5;["
after;i�;;6;70;o;;{ ;	T;i�;
[o; ;i ;["content;@;!;";o;1;@;""";;2;i�;
[ o;
;i�;@;
[ ;;;["1/* Addresses
 * ------------------------- */o;;i ;@;["address;o;;" ;i�;[o;;[o;
;@y;i�;0;[o;	;@y;0;["address;i�;o;;{ ;	T;i�;
[	o; ;i ;["display;@;!;";o;1;@;"
block;;2;i�;
[ o; ;i ;["margin-bottom;@;!;";o;.	;i�;"line-height-computed;@;/"line_height_computed;i�;
[ o; ;i ;["font-style;@;!;";o;1;@;"normal;;2;i�;
[ o; ;i ;["line-height;@;!;";o;.	;i�;"line-height-base;@;/"line_height_base;i�;
[ o;
;i�;@;
[ ;;;["-/* Links
 * ------------------------- */o;;i ;@;["a.subdued;o;;" ;i�;[o;;[o;
;@�;i�;0;[o;	;@�;0;["a;i�o;3;@�;["subdued;i�;o;;{ ;	T;i�;
[
o; ;i ;["padding-right;@;!;";o;1;@;"	10px;;2;i�;
[ o; ;i ;["
color;@;!;";o;1;@;"	#888;;2;i�;
[ o; ;i ;["text-decoration;@;!;";o;1;@;"	none;;2;i�;
[ o;;i ;@;["&:hover;o;;" ;i�;[o;;[o;
;@�;i�;0;[o;4;@�;i�o;5
;@�;["
hover;i�;;6;70;o;;{ ;	T;i�;
[o; ;i ;["text-decoration;@;!;";o;1;@;"	none;;2;i�;
[ o;;i ;@;["&:last-child;o;;" ;i�;[o;;[o;
;@�;i�;0;[o;4;@�;i�o;5
;@�;["last-child;i�;;6;70;o;;{ ;	T;i�;
[o; ;i ;["padding-right;@;!;";o;1;@;"0;;2;i�;
[ 