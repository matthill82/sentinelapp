3.2.1 (Media Mark)
19dec8c32ad3f31b92a1537a54d50b6ebeb6c3f5
o:Sass::Tree::RootNode
:
@linei:@options{ :@template"
/**
 * Checkbox
 * --------------------------------------------------
 */

.checkbox {
  // set the color defaults
  @include checkbox-style($checkbox-off-border-default, $checkbox-on-bg-default, $checkbox-on-border-default);

  position: relative;
  display: inline-block;
  padding: ($checkbox-height / 4) ($checkbox-width / 4);
  cursor: pointer;
}
.checkbox-light  {
  @include checkbox-style($checkbox-off-border-light, $checkbox-on-bg-light, $checkbox-off-border-light);
}
.checkbox-stable  {
  @include checkbox-style($checkbox-off-border-stable, $checkbox-on-bg-stable, $checkbox-off-border-stable);
}
.checkbox-positive  {
  @include checkbox-style($checkbox-off-border-positive, $checkbox-on-bg-positive, $checkbox-off-border-positive);
}
.checkbox-calm  {
  @include checkbox-style($checkbox-off-border-calm, $checkbox-on-bg-calm, $checkbox-off-border-calm);
}
.checkbox-assertive  {
  @include checkbox-style($checkbox-off-border-assertive, $checkbox-on-bg-assertive, $checkbox-off-border-assertive);
}
.checkbox-balanced  {
  @include checkbox-style($checkbox-off-border-balanced, $checkbox-on-bg-balanced, $checkbox-off-border-balanced);
}
.checkbox-energized{
  @include checkbox-style($checkbox-off-border-energized, $checkbox-on-bg-energized, $checkbox-off-border-energized);
}
.checkbox-royal  {
  @include checkbox-style($checkbox-off-border-royal, $checkbox-on-bg-royal, $checkbox-off-border-royal);
}
.checkbox-dark  {
  @include checkbox-style($checkbox-off-border-dark, $checkbox-on-bg-dark, $checkbox-off-border-dark);
}

.checkbox input:disabled:before,
.checkbox input:disabled + .checkbox-icon:before {
  border-color: $checkbox-off-border-light;
}

.checkbox input:disabled:checked:before,
.checkbox input:disabled:checked + .checkbox-icon:before {
  background: $checkbox-on-bg-light;
}


.checkbox.checkbox-input-hidden input {
  display: none !important;
}

.checkbox input,
.checkbox-icon {
  position: relative;
  width: $checkbox-width;
  height: $checkbox-height;
  display: block;
  border: 0;
  background: transparent;
  cursor: pointer;
  -webkit-appearance: none;

  &:before {
    // what the checkbox looks like when its not checked
    display: table;
    width: 100%;
    height: 100%;
    border-width: $checkbox-border-width;
    border-style: solid;
    border-radius: $checkbox-border-radius;
    background: $checkbox-off-bg-color;
    content: ' ';
    @include transition(background-color 20ms ease-in-out);
  }
}

.checkbox input:checked:before,
input:checked + .checkbox-icon:before {
  border-width: $checkbox-border-width + 1;
}

// the checkmark within the box
.checkbox input:after,
.checkbox-icon:after {
  @include transition(opacity .05s ease-in-out);
  @include rotate(-45deg);
  position: absolute;
  top: 33%;
  left: 25%;
  display: table;
  width: ($checkbox-width / 2);
  height: ($checkbox-width / 4) - 1;
  border: $checkbox-check-width solid $checkbox-check-color;
  border-top: 0;
  border-right: 0;
  content: ' ';
  opacity: 0;
}

.platform-android .checkbox-platform input:before,
.platform-android .checkbox-platform .checkbox-icon:before,
.checkbox-square input:before,
.checkbox-square .checkbox-icon:before {
  border-radius: 2px;
  width: 72%;
  height: 72%;
  margin-top: 14%;
  margin-left: 14%;
  border-width: 2px;
}

.platform-android .checkbox-platform input:after,
.platform-android .checkbox-platform .checkbox-icon:after,
.checkbox-square input:after,
.checkbox-square .checkbox-icon:after {
  border-width: 2px;
  top: 19%;
  left: 25%;
  width: ($checkbox-width / 2) - 1;
  height: 7px;
}

.grade-c .checkbox input:after,
.grade-c .checkbox-icon:after {
  @include rotate(0);
  top: 3px;
  left: 4px;
  border: none;
  color: $checkbox-check-color;
  content: '\2713';
  font-weight: bold;
  font-size: 20px;
}

// what the checkmark looks like when its checked
.checkbox input:checked:after,
input:checked + .checkbox-icon:after {
  opacity: 1;
}

// make sure item content have enough padding on left to fit the checkbox
.item-checkbox {
  padding-left: ($item-padding * 2) + $checkbox-width;

  &.active {
    box-shadow: none;
  }
}

// position the checkbox to the left within an item
.item-checkbox .checkbox {
  position: absolute;
  top: 50%;
  right: $item-padding / 2;
  left: $item-padding / 2;
  z-index: $z-index-item-checkbox;
  margin-top: (($checkbox-height + ($checkbox-height / 2)) / 2) * -1;
}


.item-checkbox.item-checkbox-right {
  padding-right: ($item-padding * 2) + $checkbox-width;
  padding-left: $item-padding;
}

.item-checkbox-right .checkbox input,
.item-checkbox-right .checkbox-icon {
  float: right;
}
:@has_childrenT:@children["o:Sass::Tree::CommentNode
;i;@;
[ :
@type:normal:@value["N/**
 * Checkbox
 * --------------------------------------------------
 */o:Sass::Tree::RuleNode:
@tabsi ;@:
@rule[".checkbox:@parsed_ruleso:"Sass::Selector::CommaSequence:@filename" ;i:@members[o:Sass::Selector::Sequence;[o:#Sass::Selector::SimpleSequence
;@;i:@subject0;[o:Sass::Selector::Class;@:
@name["checkbox;i:@sourceso:Set:
@hash{ ;	T;i;
[o;
;i;@;
[ ;:silent;["!/* set the color defaults */o:Sass::Tree::MixinNode:
@args[o:Sass::Script::Variable	;i;" checkbox-off-border-default;@:@underscored_name" checkbox_off_border_defaulto;!	;i;"checkbox-on-bg-default;@;""checkbox_on_bg_defaulto;!	;i;"checkbox-on-border-default;@;""checkbox_on_border_default;"checkbox-style;i;@;
[ :@splat0:@keywords{ o:Sass::Tree::PropNode;i ;["position;@:@prop_syntax:new;o:Sass::Script::String;@;"relative;:identifier;i;
[ o;%;i ;["display;@;&;';o;(;@;"inline-block;;);i;
[ o;%;i ;["padding;@;&;';o:Sass::Script::List	;i;@:@separator:
space;[o:Sass::Script::Operation
:@operator:div;i;@:@operand1o;!	;i;"checkbox-height;@;""checkbox_height:@operand2o:Sass::Script::Number:@numerator_units[ ;i;@:@original"4;i	:@denominator_units[ o;-
;.;/;i;@;0o;!	;i;"checkbox-width;@;""checkbox_width;1o;2;3[ ;i;@;4"4;i	;5@G;i;
[ o;%;i ;["cursor;@;&;';o;(;@;"pointer;;);i;
[ o;;i ;@;[".checkbox-light;o;;" ;i;[o;;[o;
;@Z;i;0;[o;;@Z;["checkbox-light;i;o;;{ ;	T;i;
[o;; [o;!	;i;"checkbox-off-border-light;@;""checkbox_off_border_lighto;!	;i;"checkbox-on-bg-light;@;""checkbox_on_bg_lighto;!	;i;"checkbox-off-border-light;@;""checkbox_off_border_light;"checkbox-style;i;@;
[ ;#0;${ o;;i ;@;[".checkbox-stable;o;;" ;i;[o;;[o;
;@x;i;0;[o;;@x;["checkbox-stable;i;o;;{ ;	T;i;
[o;; [o;!	;i;"checkbox-off-border-stable;@;""checkbox_off_border_stableo;!	;i;"checkbox-on-bg-stable;@;""checkbox_on_bg_stableo;!	;i;"checkbox-off-border-stable;@;""checkbox_off_border_stable;"checkbox-style;i;@;
[ ;#0;${ o;;i ;@;[".checkbox-positive;o;;" ;i;[o;;[o;
;@;i;0;[o;;@;["checkbox-positive;i;o;;{ ;	T;i;
[o;; [o;!	;i;"!checkbox-off-border-positive;@;""!checkbox_off_border_positiveo;!	;i;"checkbox-on-bg-positive;@;""checkbox_on_bg_positiveo;!	;i;"!checkbox-off-border-positive;@;""!checkbox_off_border_positive;"checkbox-style;i;@;
[ ;#0;${ o;;i ;@;[".checkbox-calm;o;;" ;i;[o;;[o;
;@¯;i;0;[o;;@¯;["checkbox-calm;i;o;;{ ;	T;i;
[o;; [o;!	;i;"checkbox-off-border-calm;@;""checkbox_off_border_calmo;!	;i;"checkbox-on-bg-calm;@;""checkbox_on_bg_calmo;!	;i;"checkbox-off-border-calm;@;""checkbox_off_border_calm;"checkbox-style;i;@;
[ ;#0;${ o;;i ;@;[".checkbox-assertive;o;;" ;i!;[o;;[o;
;@Í;i!;0;[o;;@Í;["checkbox-assertive;i!;o;;{ ;	T;i!;
[o;; [o;!	;i";""checkbox-off-border-assertive;@;"""checkbox_off_border_assertiveo;!	;i";"checkbox-on-bg-assertive;@;""checkbox_on_bg_assertiveo;!	;i";""checkbox-off-border-assertive;@;"""checkbox_off_border_assertive;"checkbox-style;i";@;
[ ;#0;${ o;;i ;@;[".checkbox-balanced;o;;" ;i$;[o;;[o;
;@ë;i$;0;[o;;@ë;["checkbox-balanced;i$;o;;{ ;	T;i$;
[o;; [o;!	;i%;"!checkbox-off-border-balanced;@;""!checkbox_off_border_balancedo;!	;i%;"checkbox-on-bg-balanced;@;""checkbox_on_bg_balancedo;!	;i%;"!checkbox-off-border-balanced;@;""!checkbox_off_border_balanced;"checkbox-style;i%;@;
[ ;#0;${ o;;i ;@;[".checkbox-energized;o;;" ;i';[o;;[o;
;@	;i';0;[o;;@	;["checkbox-energized;i';o;;{ ;	T;i';
[o;; [o;!	;i(;""checkbox-off-border-energized;@;"""checkbox_off_border_energizedo;!	;i(;"checkbox-on-bg-energized;@;""checkbox_on_bg_energizedo;!	;i(;""checkbox-off-border-energized;@;"""checkbox_off_border_energized;"checkbox-style;i(;@;
[ ;#0;${ o;;i ;@;[".checkbox-royal;o;;" ;i*;[o;;[o;
;@';i*;0;[o;;@';["checkbox-royal;i*;o;;{ ;	T;i*;
[o;; [o;!	;i+;"checkbox-off-border-royal;@;""checkbox_off_border_royalo;!	;i+;"checkbox-on-bg-royal;@;""checkbox_on_bg_royalo;!	;i+;"checkbox-off-border-royal;@;""checkbox_off_border_royal;"checkbox-style;i+;@;
[ ;#0;${ o;;i ;@;[".checkbox-dark;o;;" ;i-;[o;;[o;
;@E;i-;0;[o;;@E;["checkbox-dark;i-;o;;{ ;	T;i-;
[o;; [o;!	;i.;"checkbox-off-border-dark;@;""checkbox_off_border_darko;!	;i.;"checkbox-on-bg-dark;@;""checkbox_on_bg_darko;!	;i.;"checkbox-off-border-dark;@;""checkbox_off_border_dark;"checkbox-style;i.;@;
[ ;#0;${ o;;i ;@;["V.checkbox input:disabled:before,
.checkbox input:disabled + .checkbox-icon:before;o;;" ;i2;[o;;[o;
;@c;i2;0;[o;;@c;["checkbox;i2;o;;{ o;
;@c;i2;0;[o:Sass::Selector::Element	;@c:@namespace0;["
input;i2o:Sass::Selector::Pseudo
;@c;["disabled;i2;:
class:	@arg0o;8
;@c;["before;i2;;9;:0;o;;{ o;;[
"
o;
;@c;i2;0;[o;;@c;["checkbox;i2;o;;{ o;
;@c;i2;0;[o;6	;@c;70;["
input;i2o;8
;@c;["disabled;i2;;9;:0;o;;{ "+o;
;@c;i2;0;[o;;@c;["checkbox-icon;i2o;8
;@c;["before;i2;;9;:0;o;;{ ;	T;i2;
[o;%;i ;["border-color;@;&;';o;!	;i3;"checkbox-off-border-light;@;""checkbox_off_border_light;i3;
[ o;;i ;@;["f.checkbox input:disabled:checked:before,
.checkbox input:disabled:checked + .checkbox-icon:before;o;;" ;i7;[o;;[o;
;@¦;i7;0;[o;;@¦;["checkbox;i7;o;;{ o;
;@¦;i7;0;[	o;6	;@¦;70;["
input;i7o;8
;@¦;["disabled;i7;;9;:0o;8
;@¦;["checked;i7;;9;:0o;8
;@¦;["before;i7;;9;:0;o;;{ o;;[
"
o;
;@¦;i7;0;[o;;@¦;["checkbox;i7;o;;{ o;
;@¦;i7;0;[o;6	;@¦;70;["
input;i7o;8
;@¦;["disabled;i7;;9;:0o;8
;@¦;["checked;i7;;9;:0;o;;{ "+o;
;@¦;i7;0;[o;;@¦;["checkbox-icon;i7o;8
;@¦;["before;i7;;9;:0;o;;{ ;	T;i7;
[o;%;i ;["background;@;&;';o;!	;i8;"checkbox-on-bg-light;@;""checkbox_on_bg_light;i8;
[ o;;i ;@;["*.checkbox.checkbox-input-hidden input;o;;" ;i<;[o;;[o;
;@ï;i<;0;[o;;@ï;["checkbox;i<o;;@ï;["checkbox-input-hidden;i<;o;;{ o;
;@ï;i<;0;[o;6	;@ï;70;["
input;i<;o;;{ ;	T;i<;
[o;%;i ;["display;@;&;';o;(;@;"none !important;;);i=;
[ o;;i ;@;["$.checkbox input,
.checkbox-icon;o;;" ;iA;[o;;[o;
;@;iA;0;[o;;@;["checkbox;iA;o;;{ o;
;@;iA;0;[o;6	;@;70;["
input;iA;o;;{ o;;["
o;
;@;iA;0;[o;;@;["checkbox-icon;iA;o;;{ ;	T;iA;
[o;%;i ;["position;@;&;';o;(;@;"relative;;);iB;
[ o;%;i ;["
width;@;&;';o;!	;iC;"checkbox-width;@;""checkbox_width;iC;
[ o;%;i ;["height;@;&;';o;!	;iD;"checkbox-height;@;""checkbox_height;iD;
[ o;%;i ;["display;@;&;';o;(;@;"
block;;);iE;
[ o;%;i ;["border;@;&;';o;(;@;"0;;);iF;
[ o;%;i ;["background;@;&;';o;(;@;"transparent;;);iG;
[ o;%;i ;["cursor;@;&;';o;(;@;"pointer;;);iH;
[ o;%;i ;["-webkit-appearance;@;&;';o;(;@;"	none;;);iI;
[ o;;i ;@;["&:before;o;;" ;iK;[o;;[o;
;@b;iK;0;[o:Sass::Selector::Parent;@b;iKo;8
;@b;["before;iK;;9;:0;o;;{ ;	T;iK;
[o;
;iL;@;
[ ;;;["</* what the checkbox looks like when its not checked */o;%;i ;["display;@;&;';o;(;@;"
table;;);iM;
[ o;%;i ;["
width;@;&;';o;(;@;"	100%;;);iN;
[ o;%;i ;["height;@;&;';o;(;@;"	100%;;);iO;
[ o;%;i ;["border-width;@;&;';o;!	;iP;"checkbox-border-width;@;""checkbox_border_width;iP;
[ o;%;i ;["border-style;@;&;';o;(;@;"
solid;;);iQ;
[ o;%;i ;["border-radius;@;&;';o;!	;iR;"checkbox-border-radius;@;""checkbox_border_radius;iR;
[ o;%;i ;["background;@;&;';o;!	;iS;"checkbox-off-bg-color;@;""checkbox_off_bg_color;iS;
[ o;%;i ;["content;@;&;';o;(;@;"' ';;);iT;
[ o;; [o;*	;iU;@;+;,;[o;(	;iU;@;"background-color;;)o;2;3["ms;iU;@;4"	20ms;i;5[ o;(	;iU;@;"ease-in-out;;);"transition;iU;@;
[ ;#0;${ o;;i ;@;["J.checkbox input:checked:before,
input:checked + .checkbox-icon:before;o;;" ;iZ;[o;;[o;
;@º;iZ;0;[o;;@º;["checkbox;iZ;o;;{ o;
;@º;iZ;0;[o;6	;@º;70;["
input;iZo;8
;@º;["checked;iZ;;9;:0o;8
;@º;["before;iZ;;9;:0;o;;{ o;;[	"
o;
;@º;iZ;0;[o;6	;@º;70;["
input;iZo;8
;@º;["checked;iZ;;9;:0;o;;{ "+o;
;@º;iZ;0;[o;;@º;["checkbox-icon;iZo;8
;@º;["before;iZ;;9;:0;o;;{ ;	T;iZ;
[o;%;i ;["border-width;@;&;';o;-
;.:	plus;i[;@;0o;!	;i[;"checkbox-border-width;@;""checkbox_border_width;1o;2;3[ ;i[;@;4"1;i;5@G;i[;
[ o;
;i^;@;
[ ;;;["'/* the checkmark within the box */o;;i ;@;["0.checkbox input:after,
.checkbox-icon:after;o;;" ;i`;[o;;[o;
;@þ;i`;0;[o;;@þ;["checkbox;i`;o;;{ o;
;@þ;i`;0;[o;6	;@þ;70;["
input;i`o;8
;@þ;["
after;i`;;9;:0;o;;{ o;;["
o;
;@þ;i`;0;[o;;@þ;["checkbox-icon;i`o;8
;@þ;["
after;i`;;9;:0;o;;{ ;	T;i`;
[o;; [o;*	;ia;@;+;,;[o;(	;ia;@;"opacity;;)o;2;3["s;ia;@;4"
0.05s;f	0.05;5[ o;(	;ia;@;"ease-in-out;;);"transition;ia;@;
[ ;#0;${ o;; [o;2;3["deg;ib;@;4"-45deg;iÎ;5[ ;"rotate;ib;@;
[ ;#0;${ o;%;i ;["position;@;&;';o;(;@;"absolute;;);ic;
[ o;%;i ;["top;@;&;';o;(;@;"33%;;);id;
[ o;%;i ;["	left;@;&;';o;(;@;"25%;;);ie;
[ o;%;i ;["display;@;&;';o;(;@;"
table;;);if;
[ o;%;i ;["
width;@;&;';o;-
;.;/;ig;@;0o;!	;ig;"checkbox-width;@;""checkbox_width;1o;2;3[ ;ig;@;4"2;i;5@G;ig;
[ o;%;i ;["height;@;&;';o;-
;.:
minus;ih;@;0o;-
;.;/;ih;@;0o;!	;ih;"checkbox-width;@;""checkbox_width;1o;2;3[ ;ih;@;4"4;i	;5@G;1o;2;3[ ;ih;@;4"1;i;5@G;ih;
[ o;%;i ;["border;@;&;';o;*	;ii;@;+;,;[o;!	;ii;"checkbox-check-width;@;""checkbox_check_widtho;(	;ii;@;"
solid;;)o;!	;ii;"checkbox-check-color;@;""checkbox_check_color;ii;
[ o;%;i ;["border-top;@;&;';o;(;@;"0;;);ij;
[ o;%;i ;["border-right;@;&;';o;(;@;"0;;);ik;
[ o;%;i ;["content;@;&;';o;(;@;"' ';;);il;
[ o;%;i ;["opacity;@;&;';o;(;@;"0;;);im;
[ o;;i ;@;["´.platform-android .checkbox-platform input:before,
.platform-android .checkbox-platform .checkbox-icon:before,
.checkbox-square input:before,
.checkbox-square .checkbox-icon:before;o;;" ;is;[	o;;[o;
;@;is;0;[o;;@;["platform-android;is;o;;{ o;
;@;is;0;[o;;@;["checkbox-platform;is;o;;{ o;
;@;is;0;[o;6	;@;70;["
input;iso;8
;@;["before;is;;9;:0;o;;{ o;;[	"
o;
;@;is;0;[o;;@;["platform-android;is;o;;{ o;
;@;is;0;[o;;@;["checkbox-platform;is;o;;{ o;
;@;is;0;[o;;@;["checkbox-icon;iso;8
;@;["before;is;;9;:0;o;;{ o;;["
o;
;@;is;0;[o;;@;["checkbox-square;is;o;;{ o;
;@;is;0;[o;6	;@;70;["
input;iso;8
;@;["before;is;;9;:0;o;;{ o;;["
o;
;@;is;0;[o;;@;["checkbox-square;is;o;;{ o;
;@;is;0;[o;;@;["checkbox-icon;iso;8
;@;["before;is;;9;:0;o;;{ ;	T;is;
[o;%;i ;["border-radius;@;&;';o;(;@;"2px;;);it;
[ o;%;i ;["
width;@;&;';o;(;@;"72%;;);iu;
[ o;%;i ;["height;@;&;';o;(;@;"72%;;);iv;
[ o;%;i ;["margin-top;@;&;';o;(;@;"14%;;);iw;
[ o;%;i ;["margin-left;@;&;';o;(;@;"14%;;);ix;
[ o;%;i ;["border-width;@;&;';o;(;@;"2px;;);iy;
[ o;;i ;@;["°.platform-android .checkbox-platform input:after,
.platform-android .checkbox-platform .checkbox-icon:after,
.checkbox-square input:after,
.checkbox-square .checkbox-icon:after;o;;" ;i;[	o;;[o;
;@ ;i;0;[o;;@ ;["platform-android;i;o;;{ o;
;@ ;i;0;[o;;@ ;["checkbox-platform;i;o;;{ o;
;@ ;i;0;[o;6	;@ ;70;["
input;io;8
;@ ;["
after;i;;9;:0;o;;{ o;;[	"
o;
;@ ;i;0;[o;;@ ;["platform-android;i;o;;{ o;
;@ ;i;0;[o;;@ ;["checkbox-platform;i;o;;{ o;
;@ ;i;0;[o;;@ ;["checkbox-icon;io;8
;@ ;["
after;i;;9;:0;o;;{ o;;["
o;
;@ ;i;0;[o;;@ ;["checkbox-square;i;o;;{ o;
;@ ;i;0;[o;6	;@ ;70;["
input;io;8
;@ ;["
after;i;;9;:0;o;;{ o;;["
o;
;@ ;i;0;[o;;@ ;["checkbox-square;i;o;;{ o;
;@ ;i;0;[o;;@ ;["checkbox-icon;io;8
;@ ;["
after;i;;9;:0;o;;{ ;	T;i;
[
o;%;i ;["border-width;@;&;';o;(;@;"2px;;);i{;
[ o;%;i ;["top;@;&;';o;(;@;"19%;;);i|;
[ o;%;i ;["	left;@;&;';o;(;@;"25%;;);i};
[ o;%;i ;["
width;@;&;';o;-
;.;=;i~;@;0o;-
;.;/;i~;@;0o;!	;i~;"checkbox-width;@;""checkbox_width;1o;2;3[ ;i~;@;4"2;i;5@G;1o;2;3[ ;i~;@;4"1;i;5@G;i~;
[ o;%;i ;["height;@;&;';o;(;@;"7px;;);i;
[ o;;i ;@;["B.grade-c .checkbox input:after,
.grade-c .checkbox-icon:after;o;;" ;i;[o;;[o;
;@«;i;0;[o;;@«;["grade-c;i;o;;{ o;
;@«;i;0;[o;;@«;["checkbox;i;o;;{ o;
;@«;i;0;[o;6	;@«;70;["
input;io;8
;@«;["
after;i;;9;:0;o;;{ o;;["
o;
;@«;i;0;[o;;@«;["grade-c;i;o;;{ o;
;@«;i;0;[o;;@«;["checkbox-icon;io;8
;@«;["
after;i;;9;:0;o;;{ ;	T;i;
[o;; [o;2;3[ ;i;@;4"0;i ;5@G;"rotate;i;@;
[ ;#0;${ o;%;i ;["top;@;&;';o;(;@;"3px;;);i;
[ o;%;i ;["	left;@;&;';o;(;@;"4px;;);i;
[ o;%;i ;["border;@;&;';o;(;@;"	none;;);i;
[ o;%;i ;["
color;@;&;';o;!	;i;"checkbox-check-color;@;""checkbox_check_color;i;
[ o;%;i ;["content;@;&;';o;(;@;"'\2713';;);i;
[ o;%;i ;["font-weight;@;&;';o;(;@;"	bold;;);i;
[ o;%;i ;["font-size;@;&;';o;(;@;"	20px;;);i;
[ o;
;i;@;
[ ;;;["9/* what the checkmark looks like when its checked */o;;i ;@;["H.checkbox input:checked:after,
input:checked + .checkbox-icon:after;o;;" ;i;[o;;[o;
;@;i;0;[o;;@;["checkbox;i;o;;{ o;
;@;i;0;[o;6	;@;70;["
input;io;8
;@;["checked;i;;9;:0o;8
;@;["
after;i;;9;:0;o;;{ o;;[	"
o;
;@;i;0;[o;6	;@;70;["
input;io;8
;@;["checked;i;;9;:0;o;;{ "+o;
;@;i;0;[o;;@;["checkbox-icon;io;8
;@;["
after;i;;9;:0;o;;{ ;	T;i;
[o;%;i ;["opacity;@;&;';o;(;@;"1;;);i;
[ o;
;i;@;
[ ;;;["Q/* make sure item content have enough padding on left to fit the checkbox */o;;i ;@;[".item-checkbox;o;;" ;i;[o;;[o;
;@V;i;0;[o;;@V;["item-checkbox;i;o;;{ ;	T;i;
[o;%;i ;["padding-left;@;&;';o;-
;.;<;i;@;0o;-
;.:
times;i;@;0o;!	;i;"item-padding;@;""item_padding;1o;2;3[ ;i;@;4"2;i;5@G;1o;!	;i;"checkbox-width;@;""checkbox_width;i;
[ o;;i ;@;["&.active;o;;" ;i;[o;;[o;
;@u;i;0;[o;;;@u;io;;@u;["active;i;o;;{ ;	T;i;
[o;%;i ;["box-shadow;@;&;';o;(;@;"	none;;);i;
[ o;
;i;@;
[ ;;;[";/* position the checkbox to the left within an item */o;;i ;@;[".item-checkbox .checkbox;o;;" ;i;[o;;[o;
;@;i;0;[o;;@;["item-checkbox;i;o;;{ o;
;@;i;0;[o;;@;["checkbox;i;o;;{ ;	T;i;
[o;%;i ;["position;@;&;';o;(;@;"absolute;;);i;
[ o;%;i ;["top;@;&;';o;(;@;"50%;;);i ;
[ o;%;i ;["
right;@;&;';o;-
;.;/;i¡;@;0o;!	;i¡;"item-padding;@;""item_padding;1o;2;3[ ;i¡;@;4"2;i;5@G;i¡;
[ o;%;i ;["	left;@;&;';o;-
;.;/;i¢;@;0o;!	;i¢;"item-padding;@;""item_padding;1o;2;3[ ;i¢;@;4"2;i;5@G;i¢;
[ o;%;i ;["z-index;@;&;';o;!	;i£;"z-index-item-checkbox;@;""z_index_item_checkbox;i£;
[ o;%;i ;["margin-top;@;&;';o;-
;.;>;i¤;@;0o;-
;.;/;i¤;@;0o;-
;.;<;i¤;@;0o;!	;i¤;"checkbox-height;@;""checkbox_height;1o;-
;.;/;i¤;@;0o;!	;i¤;"checkbox-height;@;""checkbox_height;1o;2;3[ ;i¤;@;4"2;i;5@G;1o;2
;3[ ;i¤;@;i;5@G;1o;2;3[ ;i¤;@;4"-1;iú;5@G;i¤;
[ o;;i ;@;["'.item-checkbox.item-checkbox-right;o;;" ;i¨;[o;;[o;
;@æ;i¨;0;[o;;@æ;["item-checkbox;i¨o;;@æ;["item-checkbox-right;i¨;o;;{ ;	T;i¨;
[o;%;i ;["padding-right;@;&;';o;-
;.;<;i©;@;0o;-
;.;>;i©;@;0o;!	;i©;"item-padding;@;""item_padding;1o;2;3[ ;i©;@;4"2;i;5@G;1o;!	;i©;"checkbox-width;@;""checkbox_width;i©;
[ o;%;i ;["padding-left;@;&;';o;!	;iª;"item-padding;@;""item_padding;iª;
[ o;;i ;@;["N.item-checkbox-right .checkbox input,
.item-checkbox-right .checkbox-icon;o;;" ;i®;[o;;[o;
;@;i®;0;[o;;@;["item-checkbox-right;i®;o;;{ o;
;@;i®;0;[o;;@;["checkbox;i®;o;;{ o;
;@;i®;0;[o;6	;@;70;["
input;i®;o;;{ o;;["
o;
;@;i®;0;[o;;@;["item-checkbox-right;i®;o;;{ o;
;@;i®;0;[o;;@;["checkbox-icon;i®;o;;{ ;	T;i®;
[o;%;i ;["
float;@;&;';o;(;@;"
right;;);i¯;
[ 