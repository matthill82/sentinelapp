(function () {

    'use strict';

    angular.module('sentinelApp').factory('theftData', ['$http', '$q', '$ionicLoading', '$timeout', theftData]);

    function theftData($http, $q, $ionicLoading, $timeout) {

        var appHeaders = {
            headers : {
                'Content-Type' : 'application/json; charset=UTF-8'
            }
        };

        function getFeedData() {

            var vm = this;

            var deferred = $q.defer();

            vm.loadingIndicator = $ionicLoading.show({
                content: 'Loading Data',
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 400,
                showDelay: 0
            });

            $http.get("app/service/data/theftData.json", appHeaders, { cache : true } )
                .success(function (data) {
                    console.log("HTTP call went as planned");
                    $timeout(function() {
                        vm.loadingIndicator.hide();
                        deferred.resolve(data);
                    },250);
                })
                .error(function (status, error) {
                    console.log("Error while making HTTP call" + "Status is: " + status + "The error was: " + error);
                    vm.loadingIndicator.hide();
                    deferred.reject();
                });

            return deferred.promise;
        }

        function setFeedData(obj) {

            var vm = this;

            var deferred = $q.defer();

            vm.loadingIndicator = $ionicLoading.show({
                content: 'Setting Data',
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 400,
                showDelay: 0
            });

            $http.post("app/service/data/theftData.json", appHeaders, { cache : true } )
                .success(function (data) {
                    console.log("HTTP call went as planned");
                    $timeout(function() {
                        vm.loadingIndicator.hide();
                        deferred.resolve(data);
                    },250);
                })
                .error(function (status, error) {
                    console.log("Error while making HTTP call" + "Status is: " + status + "The error was: " + error);
                    vm.loadingIndicator.hide();
                    deferred.reject();
                });

            return deferred.promise;
        }

        return {
            getFeedData: getFeedData,
            setFeedData: setFeedData
        };

    }

})();