/**
 * Created by mhill168 on 29/06/2015.
 */

(function () {

    'use strict';

    angular.module('sentinelApp').controller('CarCtrl', ['$scope', '$stateParams', 'theftData', CarCtrl]);

    function CarCtrl($scope, $stateParams, theftData) {

        var vm = this;

        vm.carId = Number($stateParams.carId);
        console.log(vm.carId);

        theftData.getFeedData().then(function (data) {

            vm.car = _.chain(data.cars)
                .filter(findById)
                .map(function (item) {
                    return {
                       id: item.id,
                        title: item.title,
                        image: item.image
                    }
                })
                .value();
        });

        function findById(item) {
            return item.id === vm.carId;
        }

        vm.enableEdit = true;
        vm.value = "Edit";

        vm.toggleEdit = function () {
            console.log('toggling Edit');
            vm.enableEdit = !vm.enableEdit;
            (vm.enableEdit) ? vm.value = 'Edit' :  vm.value = 'Save';

        };

    }

})();