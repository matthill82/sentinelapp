/**
 * Created by mhill168 on 29/06/2015.
 */

(function () {

    'use strict';

    angular.module('sentinelApp').controller('PlaceCtrl', ['$scope', '$stateParams', 'theftData', PlaceCtrl]);

    function PlaceCtrl ($scope, $stateParams, theftData) {

        var vm = this;

        vm.placeId = Number($stateParams.placeId);
        console.log($stateParams);

        theftData.getFeedData().then(function (data) {

            vm.place = _.chain(data.places)
                .filter(findById)
                .map(function (item) {
                    return {
                        id: item.id,
                        name: item.name,
                        radius: item.radius,
                        lat: item.latitude,
                        long: item.longitude
                    }
                })
                .value();
        });

        function findById(item) {
            return item.id === vm.placeId;
        }

        vm.enableEdit = true;
        vm.value = "Edit";

        vm.toggleEdit = function () {
            console.log('toggling Edit');
            vm.enableEdit = !vm.enableEdit;
            (vm.enableEdit) ? vm.value = 'Edit' :  vm.value = 'Save';

        };


    }

})();
