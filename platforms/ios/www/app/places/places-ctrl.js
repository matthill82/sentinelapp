/**
 * Created by mhill168 on 29/06/2015.
 */

(function () {

    'use strict';

    angular.module('sentinelApp').controller('PlacesCtrl', ['$scope', '$ionicLoading', '$compile', 'theftData', PlacesCtrl]);

    function PlacesCtrl ($scope, $ionicLoading, $compile, theftData) {

        var vm = this;

        var mapOptions = {
            zoom: 4,
            center: new google.maps.LatLng(40.0000, -98.0000),
            mapTypeId: google.maps.MapTypeId.TERRAIN
        };
        $scope.error = "";
        $scope.map = new google.maps.Map(document.getElementById('map'),mapOptions);
        $scope.markers = [];

        var infoWindow = new google.maps.InfoWindow();

        var createMarker = function (info){

            var marker = new google.maps.Marker({
                map: $scope.map,
                position: new google.maps.LatLng(info.lat, info.long),
                title: info.city
            });

            marker.content = '<div class="infoWindowContent">' + info.desc + '</div>';

            google.maps.event.addListener(marker, 'click', function(){
                infoWindow.setContent('<h2>' + marker.title + '</h2>' + marker.content);
                infoWindow.open($scope.map, marker);
            });

            $scope.markers.push(marker);
        };

        $scope.centerOnMe = function() {
            if(!$scope.map) { return; }

            $scope.loading = $ionicLoading.show({
                content: 'Getting current location...',
                showBackdrop: false
            });

            var mapOptions = {
                zoom: 4,
                center: new google.maps.LatLng(40.0000, -98.0000),
                mapTypeId: google.maps.MapTypeId.TERRAIN
            };

            if(navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(pos) {
                        $scope.map.setCenter(new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude));
                        var myMarker = new google.maps.Marker({
                            map: $scope.map,
                            position: new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude),
                            title: "Your Location"
                        });
                        google.maps.event.addListener(myMarker, 'click', function(){
                            infoWindow.setContent('<h4>' + myMarker.title + '</h4>');
                            infoWindow.open($scope.map, myMarker);
                        });
                        $scope.loading.hide();
                    },
                    $scope.showError);
            } else {
                $scope.error = "Geolocation is not supported by this browser.";
            }

        };

        $scope.showError = function (error) {
            switch (error.code) {
                case error.PERMISSION_DENIED:
                    $scope.error = "User denied the request for Geolocation.";
                    break;
                case error.POSITION_UNAVAILABLE:
                    $scope.error = "Location information is unavailable.";
                    break;
                case error.TIMEOUT:
                    $scope.error = "The request to get user location timed out.";
                    break;
                case error.UNKNOWN_ERROR:
                    $scope.error = "An unknown error occurred.";
                    break;
            }
            $scope.$apply();
        };

        $scope.openInfoWindow = function(e, selectedMarker){
            e.preventDefault();
            google.maps.event.trigger(selectedMarker, 'click');
        };


        theftData.getFeedData().then(function (data) {

            vm.places = data.places;

            angular.forEach(vm.places, function(value, key) {
                createMarker(vm.places[key]);

            });
        });

        // Refreshing the data
        vm.doRefresh = function () {
            console.log("Refreshing the data....");
            // update the data
            theftData.getFeedData().then(function (newdata) {
                vm.places = newdata.places;
            });
            //Stop the ion-refresher from spinning
            $scope.$broadcast('scroll.refreshComplete');
        };

        vm.addSpace = function () {
            vm.markers.push({title: 'Added Space', id: 7});
        }

    }

})();
