/**
 * Created by mhill168 on 29/06/2015.
 */


(function () {

    'use strict';

    angular.module('sentinelApp').controller('ProfileCtrl', ['$scope', 'Camera', ProfileCtrl]);

    function ProfileCtrl ($scope, Camera) {

        $scope.enableEdit = true;
        $scope.value = 'Edit';

        $scope.toggleEdit = function () {
            console.log('toggling Edit');
            $scope.enableEdit = !$scope.enableEdit;
            ($scope.enableEdit) ? $scope.value = 'Edit' : $scope.value = 'Save';
        };

        $scope.urlForImage = function(imageName) {
            var name = imageName.substr(imageName.lastIndexOf('/') + 1);
            return cordova.file.dataDirectory + name;
        };

        $scope.images = [];

        $scope.getPhoto = function () {
            console.log('Getting Camera');
            Camera.getPicture().then(function (imageURI) {
                console.log(imageURI);
                $scope.lastPhoto = imageURI;
            }, function (err) {
                console.log(err)
            }, {
                quality: 75,
                targetWidth: 320,
                targetHeight: 200,
                saveToPhotoAlbum: false
            })
        };

        //$scope.takePicture = function () {
        //
        //    var config = {
        //        quality : 75,
        //        destinationType : Camera.DestinationType.DATA_URL,
        //        sourceType : Camera.PictureSourceType.CAMERA,
        //        allowEdit : true,
        //        encodingType: Camera.EncodingType.JPEG,
        //        targetWidth: 300,
        //        targetHeight: 300,
        //        popoverOptions: CameraPopoverOptions,
        //        saveToPhotoAlbum: false
        //    };
        //
        //    $cordovaCamera.getPicture(config).then(function (imageData) {
        //        onImageSuccess(imageData);
        //
        //        function onImageSuccess(fileURI) {
        //            createFileEntry(fileURI);
        //        }
        //
        //        function createFileEntry(fileURI) {
        //            window.resolveLocalFileSystemURL(fileURI, copyFile, fail);
        //        }
        //
        //        function copyFile(fileEntry) {
        //            var name = fileEntry.fullPath.substr(fileEntry.fullPath.lastIndexOf('/') + 1);
        //            var newName = makeId() + name;
        //
        //            window.resolveLocalFileSystemURL(cordova.file.dataDirectory, function (fileSystem2) {
        //                fileEntry.copyTo(fileSystem2, newName, onCopySuccess, fail);
        //            }, fail)
        //        }
        //
        //        function onCopySuccess(entry) {
        //            $scope.$apply(function () {
        //                $scope.images.push(entry.nativeURL)
        //            });
        //        }
        //
        //        function fail(error) {
        //            console.log('Fail: ' + error.code)
        //        }
        //
        //        function makeId() {
        //            var text = "";
        //            var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        //
        //            for ( var i=0; i <5; i++ ) {
        //                text += possible.charAt(Math.floor(Math.random() * possible.length))
        //            }
        //
        //            return text;
        //        }
        //
        //    }, function (err) {
        //        console.log(err);
        //    });
        //}

    }

})();

