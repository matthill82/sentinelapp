/**
 * Created by mhill168 on 01/07/2015.
 */

(function () {

    'use strict';

    angular.module('sentinelApp').controller('IntroCtrl', ['$scope', '$state', '$timeout', '$localStorage', IntroCtrl]);

    function IntroCtrl($scope, $state, $timeout, $localStorage) {

        var vm = this;
        var getUser = $localStorage.getObject('login');
        var getIntro = $localStorage.get('intro');

        var startApp = function () {

            if(!getUser) {
                return false;
            } else {
                $state.go('app.profile');

                // set a flag to say we did the intro
                $localStorage.set('didIntro', true);
            }

        };


        if(getIntro) {
            startApp();
        } else {
            $timeout(function () {
                navigator.splashscreen.hide();
            }, 750);
        }

        vm.next = function () {
            $scope.$broadcast('slideBox.nextSlide');
        };

    }

})();

