/**
 * Created by mhill168 on 26/06/2015.
 */

(function () {

    'use strict';

    angular.module('sentinelApp.storage', [])

        .factory('$localStorage', ['$window', function ($window) {
            return {
                set: function (key, value) {
                    $window.localStorage[key] = value;
                },
                get: function (key, defaultValue) {
                    return $window.localStorage[key] || defaultValue;
                },
                setObject: function (key, value) {
                    $window.localStorage[key] = JSON.stringify(value);
                },
                getObject: function (key) {
                    return JSON.parse($window.localStorage[key] || '{}');
                }
            }
        }]);

})();

