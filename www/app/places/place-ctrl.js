/**
 * Created by mhill168 on 29/06/2015.
 */

(function () {

    'use strict';

    angular.module('sentinelApp').controller('PlaceCtrl', ['$scope', '$stateParams', 'theftData', PlaceCtrl]);

    function PlaceCtrl ($scope, $stateParams, theftData) {

        var vm = this;

        vm.city = String($stateParams.city);
        console.log($stateParams);

        theftData.getFeedData().then(function (data) {

            vm.places = _.chain(data.places)
                .filter(findById)
                .value();

            console.log(vm.places);
        });


        function findById(item) {
            console.log(item);
            return item.city === vm.city;
        }

        vm.enableEdit = true;
        vm.value = "Edit";

        vm.toggleEdit = function () {
            console.log('toggling Edit');
            vm.enableEdit = !vm.enableEdit;
            (vm.enableEdit) ? vm.value = 'Edit' :  vm.value = 'Save';
        };


    }

})();
