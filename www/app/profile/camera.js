/**
 * Created by mhill168 on 29/06/2015.
 */


(function () {

    'use strict';

    angular.module('sentinelApp').factory('Camera', ['$q', Camera]);

    function Camera ($q) {
            return {
                getPicture: function(options) {
                    var q = $q.defer();

                    navigator.camera.getPicture(function(result) {
                        q.resolve(result);
                    }, function(err) {
                        q.reject(err);
                    }, options);

                    return q.promise;
                }
            }
        }

})();

