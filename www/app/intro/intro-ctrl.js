/**
 * Created by mhill168 on 01/07/2015.
 */

(function () {

    'use strict';

    angular.module('sentinelApp').controller('IntroCtrl', ['$scope', '$state', '$timeout', '$localStorage', IntroCtrl]);

    function IntroCtrl($scope, $state, $timeout, $localStorage) {

        var vm = this;
        var getUser = $localStorage.getObject('login');
        var getIntro = $localStorage.get('intro');

        var startApp = function () {

            if(!getUser) {
                return false;
            } else {
                $state.go('app.profile');

                // set a flag to say we did the intro
                $localStorage.set('didIntro', true);
            }

        };


        if(getIntro) {
            startApp();
        } else {
            $timeout(function () {
//                navigator.splashscreen.hide();
            }, 750);
        }

        vm.next = function () {
            $scope.$broadcast('slideBox.nextSlide');
        };


        // Our initial right buttons
        var rightButtons = [
            {
                content: 'Next',
                type: 'button-positive button-clear',
                tap: function(e) {
                    // Go to the next slide on tap
                    vm.next();
                }
            }
        ];

        // Our initial left buttons
        var leftButtons = [
            {
                content: 'Skip',
                type: 'button-positive button-clear',
                tap: function(e) {
                    // Start the app on tap
                    startApp();
                }
            }
        ];

        // Bind the left and right buttons to the scope
        vm.leftButtons = leftButtons;
        vm.rightButtons = rightButtons;

        // Called each time the slide changes
        vm.slideChanged = function(index) {

            // Check if we should update the left buttons
            if(index > 0) {
                // If this is not the first slide, give it a back button
                vm.leftButtons = [
                    {
                        content: 'Back',
                        type: 'button-positive button-clear',
                        tap: function(e) {
                            // Move to the previous slide
                            $scope.$broadcast('slideBox.prevSlide');
                        }
                    }
                ];
            } else {
                // This is the first slide, use the default left buttons
                vm.leftButtons = leftButtons;
            }

            // If this is the last slide, set the right button to
            // move to the app
            if(index == 2) {
                vm.rightButtons = [
                    {
                        content: 'Start using MyApp',
                        type: 'button-positive button-clear',
                        tap: function(e) {
                            startApp();
                        }
                    }
                ];
            } else {
                // Otherwise, use the default buttons
                vm.rightButtons = rightButtons;
            }
        };

    }

})();

